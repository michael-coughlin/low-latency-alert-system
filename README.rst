IGWN Low Latency Alert System
=============================

Development environment setup
-----------------------------

This project uses `Poetry`_ for packaging and virtual environment management.
First, `install Poetry`_. Then, create a virtual environment by running this
command in the directory that contains this README file::

    $ poetry install

To start a shell inside your virtual environment, run this commmand::

    $ poetry shell

Build documentation
-------------------

To build the documentation locally, run these commands inside your virtual
environment::

    $ make -C docs html

The rendered documentation will be inside the directory ``docs/_build/html``.

.. _`Poetry`: https://python-poetry.org/
.. _`install Poetry`: https://python-poetry.org/docs/#installation
